import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterviewComponent } from './interview.component';
import { PageComponent } from '../page/page.component';

const routes: Routes = [
  {
    path: '',
    component: InterviewComponent,
    children: [
      {
        path: ':path',
        component: PageComponent,
      },
      {
        path: '',
        redirectTo: 'index.md',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InterviewRoutingModule {}
