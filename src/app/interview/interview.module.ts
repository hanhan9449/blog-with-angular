import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterviewRoutingModule } from './interview-routing.module';
import { InterviewComponent } from './interview.component';
import { PageModule } from '../page/page.module';
import { MatSidenavModule } from '@angular/material/sidenav';
import { FlexLayoutModule } from '@angular/flex-layout';
import { InterviewNavListComponent } from '../interview-nav-list/interview-nav-list.component';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [InterviewComponent, InterviewNavListComponent],
  imports: [
    CommonModule,
    InterviewRoutingModule,
    PageModule,
    MatSidenavModule,
    FlexLayoutModule,
    MatListModule,
    MatButtonModule,
    SharedModule,
  ],
})
export class InterviewModule {}
