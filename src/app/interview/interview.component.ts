import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss'],
})
export class InterviewComponent implements OnInit {
  test!: string;

  constructor() {}

  ngOnInit(): void {
    this.test = 'console.log("hello world")';
  }
}
