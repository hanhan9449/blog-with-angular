import { Component } from '@angular/core';
import { ROUTE_ANIMATION } from './app.animation';
import { Data, RouterOutlet } from '@angular/router';

@Component({
  animations: [ROUTE_ANIMATION],
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'blog-with-angular';

  getRouteAnimation(outlet: RouterOutlet): Data {
    return outlet && outlet.activatedRouteData;
  }
}
