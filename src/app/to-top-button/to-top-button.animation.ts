import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const SHOW_BUTTON = trigger('buttonAnimation', [
  transition(':enter', [
    style({ opacity: 0 }),
    animate('200ms ease-in-out', style({ opacity: 1 })),
  ]),
  transition(':leave', [
    style({ opacity: 1 }),
    animate('200ms ease-in-out', style({ opacity: 0 })),
  ]),
]);
