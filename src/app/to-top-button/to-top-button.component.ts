import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { SHOW_BUTTON } from './to-top-button.animation';

@Component({
  selector: 'app-to-top-button',
  animations: [SHOW_BUTTON],
  templateUrl: './to-top-button.component.html',
  styleUrls: ['./to-top-button.component.scss'],
})
export class ToTopButtonComponent implements OnInit {
  scrolled = false;

  constructor(@Inject(DOCUMENT) private document: Document) {}

  ngOnInit(): void {}

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    if (document.documentElement.scrollTop > 300) {
      this.scrolled = true;
    } else if (document.documentElement.scrollTop < 100) {
      this.scrolled = false;
    }
  }
}
