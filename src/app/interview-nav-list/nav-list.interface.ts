export interface NavListInterface {
  typeName: string;
  items: NavItemInterface[];
}

export interface NavItemInterface {
  name: string;
  fileName: string;
}
