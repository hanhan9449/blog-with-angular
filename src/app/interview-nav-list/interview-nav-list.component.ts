import { Component, OnInit } from '@angular/core';
import { InterviewListService } from '../interview-list.service';
import { NavItemInterface, NavListInterface } from './nav-list.interface';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-interview-nav-list',
  templateUrl: './interview-nav-list.component.html',
  styleUrls: ['./interview-nav-list.component.scss'],
})
export class InterviewNavListComponent implements OnInit {
  list!: Observable<NavListInterface[]>;

  constructor(public interviewListService: InterviewListService) {}

  ngOnInit(): void {
    this.list = this.interviewListService.getInterviewList();
  }
}
