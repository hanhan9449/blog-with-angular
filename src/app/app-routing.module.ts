import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InterviewModule } from './interview/interview.module';
import { AppShellComponent } from './app-shell/app-shell.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  {
    path: 'shell',
    component: AppShellComponent,
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'about-me',
    loadChildren: () =>
      import('./about-me/about-me.module').then((m) => m.AboutMeModule),
  },
  {
    path: 'interview',
    loadChildren: () =>
      import('./interview/interview.module').then((m) => m.InterviewModule),
  },
  {
    path: 'learning-guide',
    loadChildren: () =>
      import('./learning-guide/learning-guide.module').then(
        (m) => m.LearningGuideModule
      ),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
