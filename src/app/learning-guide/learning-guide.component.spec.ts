import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningGuideComponent } from './learning-guide.component';

describe('LearningGuideComponent', () => {
  let component: LearningGuideComponent;
  let fixture: ComponentFixture<LearningGuideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LearningGuideComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
