import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LearningGuideRoutingModule } from './learning-guide-routing.module';
import { LearningGuideComponent } from './learning-guide.component';

@NgModule({
  declarations: [LearningGuideComponent],
  imports: [CommonModule, LearningGuideRoutingModule],
})
export class LearningGuideModule {}
