import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LearningGuideComponent } from './learning-guide.component';

const routes: Routes = [
  {
    path: '',
    component: LearningGuideComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LearningGuideRoutingModule {}
