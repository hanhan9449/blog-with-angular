import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavListInterface } from './interview-nav-list/nav-list.interface';
import { Observable } from 'rxjs';
import { ARTICLE_URL } from './shared/shared.module';

@Injectable({
  providedIn: 'root',
})
export class InterviewListService {
  constructor(
    public httpClient: HttpClient,
    @Inject(ARTICLE_URL) private articleUrl: string
  ) {}

  getInterviewList(): Observable<NavListInterface[]> {
    return this.httpClient.get<NavListInterface[]>(
      this.articleUrl + 'route.json'
    );
  }
}
