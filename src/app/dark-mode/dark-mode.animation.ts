import {
  animate,
  animation,
  state,
  style,
  transition,
  trigger,
  useAnimation,
} from '@angular/animations';

export const modeSwitchAnimation = trigger('showHidden', [
  state(
    'show',
    style({
      opacity: 1,
    })
  ),
  state(
    'hidden',
    style({
      opacity: 0,
    })
  ),
  transition('show => hidden', [animate('0.5s')]),
  transition('hidden => show', [animate('0.5s')]),
]);
