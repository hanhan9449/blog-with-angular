import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DarkModeComponent } from './dark-mode.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [DarkModeComponent],
  exports: [DarkModeComponent],
  imports: [CommonModule, MatButtonModule, MatIconModule],
})
export class DarkModeModule {}
