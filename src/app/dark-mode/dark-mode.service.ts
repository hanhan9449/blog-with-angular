import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { DarkModeType } from './dark-mode.type';

@Injectable({
  providedIn: 'root',
})
export class DarkModeService {
  currentMode$ = new BehaviorSubject<DarkModeType>('light');

  constructor() {}

  setMode(mode: DarkModeType): void {
    this.currentMode$.next(mode);
  }
}
