import { Component, OnInit } from '@angular/core';
import { DarkModeService } from './dark-mode.service';
import { Observable, tap } from 'rxjs';
import { DarkModeType } from './dark-mode.type';
import { modeSwitchAnimation } from './dark-mode.animation';

@Component({
  selector: 'app-dark-mode',
  templateUrl: './dark-mode.component.html',
  styleUrls: ['./dark-mode.component.scss'],
})
export class DarkModeComponent implements OnInit {
  currentMode$?: Observable<DarkModeType>;

  constructor(private readonly service: DarkModeService) {}

  ngOnInit(): void {
    this.currentMode$ = this.service.currentMode$.pipe(
      tap((a) => {
        switch (a) {
          case 'dark': {
            document.body.setAttribute('data-dark-mode', '1');
            return;
          }
          case 'light':
          default: {
            document.body.setAttribute('data-dark-mode', '0');
          }
        }
      })
    );
  }

  setLightMode(): void {
    this.service.setMode('light');
  }

  setDarkMode(): void {
    this.service.setMode('dark');
  }
}
