import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NavItem } from './header/nav-item.interface';

@Injectable({
  providedIn: 'root',
})
export class HeaderService {
  constructor() {}

  getNavList$(): Observable<NavItem[]> {
    const list: NavItem[] = [
      {
        linkName: '博客文章',
        relativeUrl: 'interview',
      },
      {
        linkName: '学习指南',
        relativeUrl: 'learning-guide',
      },
      {
        linkName: '关于我',
        relativeUrl: 'about-me',
      },
    ];
    return of(list);
  }
}
