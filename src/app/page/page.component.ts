import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { ARTICLE_URL } from '../shared/shared.module';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PageComponent implements OnInit {
  path = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    @Inject(ARTICLE_URL) private articleUrl: string
  ) {}

  ngOnInit(): void {
    this.route.paramMap
      .pipe(
        map((params) => {
          return params.get('path') as string;
        }),
        // TODO: 优化
        map((path: string) => this.articleUrl + path)
      )
      .subscribe((path) => (this.path = path));
  }
}
