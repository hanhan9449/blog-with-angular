import { Component, OnInit } from '@angular/core';
import { BLOG_SUBTITLE, BLOG_TITLE } from '../shared/constant';
import { NavItem } from './nav-item.interface';
import { HttpClient } from '@angular/common/http';
import { HeaderService } from '../header.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  title = BLOG_TITLE;
  subtitle = BLOG_SUBTITLE;
  navItems$?: Observable<NavItem[]>;

  constructor(
    private httpClient: HttpClient,
    private headerService: HeaderService
  ) {}

  ngOnInit(): void {
    this.navItems$ = this.headerService.getNavList$();
  }
}
