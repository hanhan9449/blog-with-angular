export interface NavItem {
  linkName: string;
  relativeUrl: string;
}
