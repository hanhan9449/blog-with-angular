import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appToTop]',
})
export class ToTopDirective {
  constructor() {}

  @HostListener('click')
  goToTop(): void {
    window.scrollTo(0, 0);
  }
}
