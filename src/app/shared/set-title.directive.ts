import {
  Directive,
  ElementRef,
  HostListener,
  Input,
  ViewChild,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { BLOG_TITLE } from './constant';

@Directive({
  selector: '[appSetTitle]',
})
export class SetTitleDirective {
  @Input('appSetTitle') title?: string;

  constructor(private titleService: Title, private el: ElementRef) {}

  @HostListener('click')
  onClick(): void {
    if (this.title) {
      this.titleService.setTitle(this.title + ' | ' + BLOG_TITLE);
    } else {
      this.titleService.setTitle(BLOG_TITLE);
    }
  }
}
