import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SetTitleDirective } from './set-title.directive';
import { ToTopDirective } from './to-top.directive';

export const ARTICLE_URL = Symbol();

@NgModule({
  declarations: [SetTitleDirective, ToTopDirective],
  imports: [CommonModule],
  providers: [
    { provide: ARTICLE_URL, useValue: 'https://article.justjs.xyz/' },
  ],
  exports: [SetTitleDirective, ToTopDirective],
})
export class SharedModule {}
