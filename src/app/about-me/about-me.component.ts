import {
  AfterContentChecked,
  AfterContentInit,
  AfterViewChecked,
  AfterViewInit,
  Component,
  DoCheck,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Gender, MeInterface } from './me.interface';

@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss'],
})
export class AboutMeComponent
  implements
    OnChanges,
    OnInit,
    OnDestroy,
    AfterContentChecked,
    AfterContentInit,
    AfterViewChecked,
    AfterViewInit,
    DoCheck {
  constructor() {}

  me!: MeInterface;

  ngOnInit(): void {
    this.me = {
      name: '韩涵',
      birthday: new Date(2000, 5, 4),
      description: '我会努力的，秋招去字节！！！！！',
      gender: Gender.MALE,
      interest: ['只想去字节跳动吃饭'],
      work: '想去字节的程序员',
      graduateSchool: '成都大学',
    };
  }

  ngAfterContentChecked(): void {}

  ngAfterContentInit(): void {}

  ngAfterViewChecked(): void {}

  ngAfterViewInit(): void {}

  ngOnDestroy(): void {}

  ngOnChanges(changes: SimpleChanges): void {}

  ngDoCheck(): void {}
}
