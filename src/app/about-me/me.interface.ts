export interface MeInterface {
  name: string;
  birthday?: Date;
  gender?: Gender;
  description?: string;
  interest?: string[];
  work?: string;
  graduateSchool?: string;
}

export enum Gender {
  MALE = 'male',
  FEMALE = 'female',
}
